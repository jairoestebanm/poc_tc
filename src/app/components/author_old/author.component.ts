import { Component, OnInit } from '@angular/core';
import { AuthorService } from '../../services/author.service';
import { Author } from './author';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css'],
  providers:[AuthorService]
})

export class AuthorComponent implements OnInit {
  columnDefs = [
    {headerName: 'ID', field:'id', sortable:true},
    {headerName: 'Age', field:'age', sortable:true},
    {headerName: 'Name', field:'name', sortable:true}
  ];
  
  public rowData: Array<any>;
  public authors: Array<Author>;

  constructor(private authorService: AuthorService) { this.authors = new Array<Author>(); this.rowData = new Array<any>();}

  ngOnInit() {
     this.authorService.getAuthors().subscribe(
       result=>{
         console.log(result);
         this.rowData = result;
       }
     )
  }

}
