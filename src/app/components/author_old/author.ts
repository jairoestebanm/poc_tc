export class Author{
    constructor(
        public id:number,
        public age:number,
        public name:string
    ){}
}