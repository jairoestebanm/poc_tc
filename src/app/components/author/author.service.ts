import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Author } from './author.model';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  base_url: String;

  userPath: String = 'author';

  constructor(private _http: HttpClient) {
    this.base_url = 'http://localhost:3000/';
  }

  public getAuthors(): Observable<any>{

    return this._http.get(`${this.base_url}${this.userPath}`)
    .map(resp=>resp);
  }

  public getAuthorById(id:number): Observable<any>{

    return this._http.get(`${this.base_url}${this.userPath}/${id}`)
    .map(resp=>resp);
  }

  public createAuthor(author: Author): Observable<any>{

    return this._http.post(`${this.base_url}${this.userPath}`, author)
    .map(resp=>resp);
  }

  public updateAuthor(author: Author): Observable<any>{

    return this._http.put(`${this.base_url}${this.userPath}/${author.id}`, author)
    .map(resp=>resp);
  }

  public deleteAuthor(id: number): Observable<any>{

    return this._http.delete(`${this.base_url}${this.userPath}/${id}`)
    .map(resp=>resp);
  }
}
