import { Component, OnInit } from '@angular/core';

import { Store, select } from '@ngrx/store';
import { observable, from, Observable, config } from 'rxjs';

import * as authorActions from '../state/author.actions';
import * as fromAuthor from '../state/author.reducer';
import { Author } from '../author.model';

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.css']
})
export class AuthorListComponent implements OnInit {

  columnDefs = [
    {headerName: 'ID', field:'id', sortable:true, checkboxSelection: true },
    {headerName: 'Age', field:'age', sortable:true},
    {headerName: 'Name', field:'name', sortable:true}
  ];

  authors$: Observable<Author[]>;
  error$: Observable<string>;

  constructor(private autor: Store<fromAuthor.AppState>) { }

  ngOnInit() {
    this.autor.dispatch(new authorActions.LoadAuthors());
    this.authors$ = this.autor.pipe(select(fromAuthor.getAuthors));
    this.error$ = this.autor.pipe(select(fromAuthor.getError));
  }

  deleteAuthor(author: Author){
    if(confirm("Are you shure You want to delete this Author?")){
      this.autor.dispatch(new authorActions.DeleteAuthor(author.id));
    }
  }

  editAuthor(author: Author){
    this.autor.dispatch(new authorActions.LoadAuthor(author.id));
  }

}
