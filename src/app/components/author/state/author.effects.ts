import { Injectable } from '@angular/core'; 

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';

import { Observable, of } from 'rxjs';
import { map, mergeMap, catchError} from 'rxjs/operators';

import { AuthorService } from '../author.service';
import * as authorActions from './author.actions';
import { Author } from '../author.model';

@Injectable()
export class AuthorEffect {
    
    constructor(
        private actions$: Actions,
        private authorService: AuthorService
    ){}

    @Effect()
    loadAuthors$: Observable<Action> = this.actions$.pipe(
        ofType<authorActions.LoadAuthors>(
            authorActions.AuthorActionTypes.LOAD_AUTHORS
        ),
        mergeMap((action: authorActions.LoadAuthors)=>
            this.authorService.getAuthors().pipe(
                map(
                    (authors: Author[]) => new authorActions.LoadAuthorsSuccess(authors)
                ),
                catchError(err => of(new authorActions.LoadAuthorsFail(err)))
            )
        )
    );


    @Effect()
    loadAuthor$: Observable<Action> = this.actions$.pipe(
        ofType<authorActions.LoadAuthor>(
            authorActions.AuthorActionTypes.LOAD_AUTHOR
        ),
        mergeMap((action: authorActions.LoadAuthor)=>
            this.authorService.getAuthorById(action.payload).pipe(
                map(
                    (author: Author) => new authorActions.LoadAuthorSuccess(author)
                ),
                catchError(err => of(new authorActions.LoadAuthorFail(err)))
            )
        )
    );

    @Effect()
    createAuthor$: Observable<Action> = this.actions$.pipe(
        ofType<authorActions.CreateAuthor>(
            authorActions.AuthorActionTypes.CREATE_AUTHOR
        ),
        map((action: authorActions.CreateAuthor) => action.payload),
        mergeMap( (author: Author) =>
            this.authorService.createAuthor(author).pipe(
                map(
                    (newAuthor: Author) => new authorActions.CreateAuthorSuccess(newAuthor)
                ),
                catchError(err => of(new authorActions.CreateAuthorFail(err)))
            )
        )
    );

    @Effect()
    updateAuthor$: Observable<Action> = this.actions$.pipe(
        ofType<authorActions.UpdateAuthor>(
            authorActions.AuthorActionTypes.UPDATE_AUTHOR
        ),
        map((action: authorActions.UpdateAuthor) => action.payload),
        mergeMap( (author: Author) =>
            this.authorService.updateAuthor(author).pipe(
                map(
                    (updateAuthor: Author) => new authorActions.UpdateAuthorSuccess({
                        id: updateAuthor.id,
                        changes: updateAuthor
                    })
                ),
                catchError(err => of(new authorActions.UpdateAuthorFail(err)))
            )
        )
    );

    @Effect()
    deleteAuthor$: Observable<Action> = this.actions$.pipe(
        ofType<authorActions.DeleteAuthor>(
            authorActions.AuthorActionTypes.DELETE_AUTHOR
        ),
        map((action: authorActions.DeleteAuthor) => action.payload),
        mergeMap( (id: number) =>
            this.authorService.deleteAuthor(id).pipe(
                map(() => new authorActions.DeleteAuthorSuccess(id)),
                catchError(err => of(new authorActions.DeleteAuthorFail(err)))
            )
        )
    );
}