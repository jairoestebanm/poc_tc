import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Author } from '../author.model';

export enum AuthorActionTypes {
    LOAD_AUTHORS = "[Author] Load authors",
    LOAD_AUTHORS_SUCCESS = "[Author] Load authors Success",
    LOAD_AUTHORS_FAIL = "[Author] Load authors Fail",
    LOAD_AUTHOR = "[Author] Load author",
    LOAD_AUTHOR_SUCCESS = "[Author] Load author Success",
    LOAD_AUTHOR_FAIL = "[Author] Load author Fail",
    CREATE_AUTHOR = "[Author] Create author",
    CREATE_AUTHOR_SUCCESS = "[Author] Create author Success",
    CREATE_AUTHOR_FAIL = "[Author] Create author Fail",
    UPDATE_AUTHOR = "[Author] Update author",
    UPDATE_AUTHOR_SUCCESS = "[Author] Update author Success",
    UPDATE_AUTHOR_FAIL = "[Author] Update author Fail",
    DELETE_AUTHOR = "[Author] Delete author",
    DELETE_AUTHOR_SUCCESS = "[Author] Delete author Success",
    DELETE_AUTHOR_FAIL = "[Author] Delete author Fail",
}

export class LoadAuthors implements Action {
    readonly type = AuthorActionTypes.LOAD_AUTHORS;
}

export class LoadAuthorsSuccess implements Action {
    readonly type = AuthorActionTypes.LOAD_AUTHORS_SUCCESS;

    constructor(public payload: Author[]) {}
}

export class LoadAuthorsFail implements Action {
    readonly type = AuthorActionTypes.LOAD_AUTHORS_FAIL;

    constructor(public payload: string) {}
}

export class LoadAuthor implements Action {
    readonly type = AuthorActionTypes.LOAD_AUTHOR;
    constructor(public payload: number) {};
}

export class LoadAuthorSuccess implements Action {
    readonly type = AuthorActionTypes.LOAD_AUTHOR_SUCCESS;

    constructor(public payload: Author) {}
}

export class LoadAuthorFail implements Action {
    readonly type = AuthorActionTypes.LOAD_AUTHOR_FAIL;

    constructor(public payload: string) {}
}

export class CreateAuthor implements Action {
    readonly type = AuthorActionTypes.CREATE_AUTHOR;
    constructor(public payload: Author) {};
}

export class CreateAuthorSuccess implements Action {
    readonly type = AuthorActionTypes.CREATE_AUTHOR_SUCCESS;

    constructor(public payload: Author) {}
}

export class CreateAuthorFail implements Action {
    readonly type = AuthorActionTypes.CREATE_AUTHOR_FAIL;

    constructor(public payload: string) {}
}

export class UpdateAuthor implements Action {
    readonly type = AuthorActionTypes.UPDATE_AUTHOR;
    constructor(public payload: Author) {};
}

export class UpdateAuthorSuccess implements Action {
    readonly type = AuthorActionTypes.UPDATE_AUTHOR_SUCCESS;

    constructor(public payload: Update<Author>) {}
}

export class UpdateAuthorFail implements Action {
    readonly type = AuthorActionTypes.UPDATE_AUTHOR_FAIL;

    constructor(public payload: string) {}
}

export class DeleteAuthor implements Action {
    readonly type = AuthorActionTypes.DELETE_AUTHOR;
    constructor(public payload: number) {};
}

export class DeleteAuthorSuccess implements Action {
    readonly type = AuthorActionTypes.DELETE_AUTHOR_SUCCESS;

    constructor(public payload: number) {}
}

export class DeleteAuthorFail implements Action {
    readonly type = AuthorActionTypes.DELETE_AUTHOR_FAIL;

    constructor(public payload: string) {}
}

export type Action = 
LoadAuthors | 
LoadAuthorsSuccess | 
LoadAuthorsFail | 
LoadAuthor | 
LoadAuthorSuccess | 
LoadAuthorFail |
CreateAuthor |
CreateAuthorSuccess |
CreateAuthorFail |
UpdateAuthor |
UpdateAuthorSuccess |
UpdateAuthorFail |
DeleteAuthor |
DeleteAuthorSuccess |
DeleteAuthorFail;