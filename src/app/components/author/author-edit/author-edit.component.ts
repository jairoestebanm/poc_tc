import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Store } from '@ngrx/store';

import { Observable } from 'rxjs';
import * as authorActions from '../state/author.actions';
import * as fromAuthor from '../state/author.reducer';
import { Author } from '../author.model';

@Component({
  selector: 'app-author-edit',
  templateUrl: './author-edit.component.html',
  styleUrls: ['./author-edit.component.css']
})
export class AuthorEditComponent implements OnInit {

  authorForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store<fromAuthor.AppState>
  ) { }

  ngOnInit() {
    this.authorForm = this.fb.group({
      id: ["", Validators.required],
      age: ["", Validators.required],
      name: ["", Validators.required]
    });

    const author$: Observable<Author> = this.store.select(
      fromAuthor.getCurrentAuthor
    );

    author$.subscribe(currentAuthor => {
      if(currentAuthor){
        this.authorForm.patchValue({
          id: currentAuthor.id,
          age: currentAuthor.age,
          name: currentAuthor.name
        });
      }
    });
  }

  editAuthor(){
    const updateAuthor: Author = {
      id: this.authorForm.get('id').value,
      age: this.authorForm.get('age').value,
      name: this.authorForm.get('name').value
    };

    this.store.dispatch(new authorActions.UpdateAuthor(updateAuthor));
  }

}
