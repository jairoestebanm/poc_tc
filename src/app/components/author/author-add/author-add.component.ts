import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Store } from '@ngrx/store';

import * as authorActions from '../state/author.actions';
import * as fromAuthor from '../state/author.reducer';
import { Author } from '../author.model';

@Component({
  selector: 'app-author-add',
  templateUrl: './author-add.component.html',
  styleUrls: ['./author-add.component.css']
})
export class AuthorAddComponent implements OnInit {

  authorForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store<fromAuthor.AppState>
  ) { }

  ngOnInit() {
    this.authorForm = this.fb.group({
      id: ["", Validators.required],
      age: ["", Validators.required],
      name: ["", Validators.required]
    });
  }

  createAuthor(){
    const newAuthor: Author = {
      id: this.authorForm.get('id').value,
      age: this.authorForm.get('age').value,
      name: this.authorForm.get('name').value
    };

    this.store.dispatch(new authorActions.CreateAuthor(newAuthor));
    this.authorForm.reset();
  }

}
