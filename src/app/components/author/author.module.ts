import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AgGridModule } from 'ag-grid-angular';

import { EffectsModule, Actions} from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { authorReducer } from './state/author.reducer';
import { AuthorEffect } from './state/author.effects';

import { AuthorComponent } from './author/author.component';
import { AuthorAddComponent } from './author-add/author-add.component';
import { AuthorEditComponent } from './author-edit/author-edit.component';
import { AuthorListComponent } from './author-list/author-list.component';

const authorRoutes:Routes = [{ path :'', component: AuthorComponent }];

@NgModule({
  declarations: [AuthorComponent, AuthorAddComponent, AuthorEditComponent, AuthorListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(authorRoutes),
    ReactiveFormsModule,
    FormsModule,
    AgGridModule.withComponents([]),
    StoreModule.forFeature("authors", authorReducer),
    EffectsModule.forFeature([AuthorEffect])
  ]
})
export class AuthorModule { }
