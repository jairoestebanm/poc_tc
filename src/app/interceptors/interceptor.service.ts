import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor{
  
  constructor() { }
  
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'token-user': 'ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKU1V6STFOaUlzSW1wMGFTSTZJakEzTXpka05qTmlPV1ZtTURJd1lXWmlOamxoT1RGaVl6QmtPR00zTVRka09EUTROV1ZqWm1ZMlptVTVaVFppWlRGa1kyVXdOR1UxWm1FME0yVmxPR0ZrWVdaaE9XUXdOVFJpTkRWaE5XRmxJbjAuZXlKaGRXUWlPaUl4SWl3aWFuUnBJam9pTURjek4yUTJNMkk1WldZd01qQmhabUkyT1dFNU1XSmpNR1E0WXpjeE4yUTRORGcxWldObVpqWm1aVGxsTm1KbE1XUmpaVEEwWlRWbVlUUXpaV1U0WVdSaFptRTVaREExTkdJME5XRTFZV1VpTENKcFlYUWlPakUxTlRrNU16WTFNVE1zSW01aVppSTZNVFUxT1Rrek5qVXhNeXdpWlhod0lqb3hOVGt4TlRVNE9URXpMQ0p6ZFdJaU9pSTRJaXdpYzJOdmNHVnpJanBiWFgwLlM3c3FQU2pvclBGYWxzT285aDl4NDBLOE8zLXhpVlNfanNZd2J2aFUxTEo3ZXNBZVZyNHZpYlh3X2tLMjVmUW5ITEszZ3dfSmpOU0Rkc1plWEZtZkRDNE9yRkxUa05rVDJKZnFfdk1KRHJPM0pjdDBnblk0Z3lYY3VqeWlQZHdyUmxEUmNCdnJkZDI0RF9QN21VWTY2dUowWlQyNURfRXBVMDlMbHh1M3pBVGh0d244eGN4ZENZcmF5SlZ4d3ZfYjN1QXJodC1FMFd1NUloTWR0RU1QMVNPSWxMVE5laUxqa1c1V2tSNXZSd0xzbkRnVVpYX2FScUVpMEFWYXZOdEFlZnBGWWNweUFteUZ3Q0lqV1hyMG9WSzhmcW5HdzJJVFFVZFhYLWZwdmR2WXJqcGpvUEt5QVZCV2J0aXBJWnF0OVdNTmtZS1E0bFpPWm4tdWF4MjhZNThKbS0tUWh6M1gxeDFOMFlEa3BuY1V2RWtIOEdnb2Q5TXhCd0ZjT3lJMzloWmhvV2dOdnRJZENLb3RJSWdnRGFRUlpjTDFLNkFIc1M1MEJncFV0dFM4UDJJM2hPVVVFYUdYclVxODZ4Zmx1ay1ocEk0S0JJbzludWNBSHdTRzgwMjU0N24tYmpJMkV2aVJJMTRmTFZrYUFFNGczZTJwY29MWXNBUTY2dGgxTkZBU2xrcG1Ndy14U3NrSV9MMUF6ZlFLa3F6ZTZ1Vy01RkZVRjctSlUyMTN2b2MxNzhVR1ZMNk45QldyaTNfaTBGTVpnUnYzbENDQ0k1bzUzdkFiR0tqVnB2VllNd1Y1Q0oxTW5mczBVbk4tMjNNWWtqYlVmN0NiT0F2NlBOb2FrMElYYlZPOFJqeWtDb3UyTkw3d0Z6SklDY05jbFVrLUNfY21vOFZLeXg0'
    });

    const reqClone = req.clone({
      headers
    });

    return next.handle(reqClone).pipe(
      catchError(this.manejarError)
    );
    
    return next.handle(reqClone);
  }

  manejarError(error: HttpErrorResponse){
    console.log('Sucedio un error');
    console.log('Registrado en el log file');
    console.warn(error);
    return throwError('Error personalizado');
  }
  
}
