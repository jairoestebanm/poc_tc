import { Action } from '@ngrx/store';
import { LIST } from './app.actions';

export function listReducer(state: any=null, action:Action){
    switch(action.type){
        case LIST:
            return state;

        default:
            return state;
    }
}