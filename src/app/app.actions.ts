import { Action } from '@ngrx/store';

export const LIST = '[Author] List';

export class ListAuthorAction implements Action{
    readonly type = LIST;
}