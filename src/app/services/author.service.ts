import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  base_url: String;

  userPath: String = 'author';

  constructor(private _http: HttpClient) {
    this.base_url = 'http://localhost:3000/';
  }

  public getAuthors(): Observable<any>{

    return this._http.get(`${this.base_url}${this.userPath}`)
    .map(resp=>resp);
  }
}
